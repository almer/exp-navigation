// point class
export class Point {
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }

  // set new position
  setPosition(x, y) {
    this.x = x;
    this.y = y;
  }
}

// vector class
export class Vector {
  constructor(pointA = new Point(), pointB = new Point()) {
    this.x = pointB.x - pointA.x;
    this.y = pointB.y - pointA.y;
  }

  // get magnitude
  get magnitude() {
    return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
  }

  // get direction
  get direction() {
    return Math.atan2(this.y, this.x);
  }

  // get unit direction
  get unit() {
    const magnitude = this.magnitude;
    if (magnitude === 0)
      throw new Error("cannot get unit vector of zero vector");
    return new Vector(
      new Point(),
      new Point(this.x / magnitude, this.y / magnitude)
    );
  }

  subtract(otherVector) {
    return new Vector(
      new Point(),
      new Point(this.x - otherVector.x, this.y - otherVector.y)
    );
  }

  add(otherVector) {
    return new Vector(
      new Point(),
      new Point(this.x + otherVector.x, this.y + otherVector.y)
    );
  }

  scale(factor) {
    return new Vector(new Point(), new Point(this.x * factor, this.y * factor));
  }
}
