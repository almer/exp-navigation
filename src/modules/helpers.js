// map value range
export function map(num, in_min, in_max, out_min, out_max) {
  return ((num - in_min) * (out_max - out_min)) / (in_max - in_min) + out_min;
}

// find nested object with key value pair
// https://stackoverflow.com/questions/45336281/javascript-find-by-value-deep-in-a-nested-object-array
export function findNestedObject(obj, key, value) {
  // Base case
  if (obj[key] === value) {
    return obj;
  } else {
    var keys = Object.keys(obj); // add this line to iterate over the keys

    for (var i = 0, len = keys.length; i < len; i++) {
      var k = keys[i]; // use this key for iteration, instead of index "i"

      // add "obj[k] &&" to ignore null values
      if (obj[k] && typeof obj[k] == "object") {
        var found = findNestedObject(obj[k], key, value);
        if (found) {
          // If the object was found in the recursive call, bubble it up.
          return found;
        }
      }
    }
  }
}

// clamp number
export function clamp(num, min, max) {
  return Math.min(Math.max(num, min), max);
}