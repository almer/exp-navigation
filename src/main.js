import * as helpers from "./modules/helpers.js";
import { Point, Vector } from "./modules/Vector.js";

const config = {
  controls: {
    mouse: true,
    button: true,
    voice: false,
    motion: false,
  },
};

const navigation = {
  home: {
    label: "Home",
    path: "/",
    command: "home",
    submenu: {
      menu: {
        label: "Menu",
        path: "/menu",
        command: "menu",
        submenu: {
          call: {
            label: "Call",
            path: "/menu/call",
            command: "call",
            submenu: {
              emergency: {
                label: "Emergency",
                command: "emergency",
                path: "/menu/call/emergency",
                message: "&#128077; Calling Emergency",
              },
              contacta: {
                label: "Flute Inc.",
                command: "contact a",
                path: "/menu/call/contacta",
                message: "&#128077; Calling Flute Inc.",
              },
              contactb: {
                label: "Brays AG",
                command: "contact b",
                path: "/menu/call/contactb",
                message: "&#128077; Calling Brays AG",
              },
            },
          },
          check: {
            label: "Checklist",
            path: "/menu/check",
            command: "checklist",
            submenu: {
              daily: {
                label: "Daily",
                command: "daily",
                path: "/menu/check/daily",
                message: "&#128077; Displaying Daily Checklist",
              },
              weekly: {
                label: "Weekly",
                command: "weekly",
                path: "/menu/check/weekly",
                message: "&#128077; Displaying Weekly Checklist",
              },
              monthly: {
                label: "Monthly",
                command: "monthly",
                path: "/menu/check/monthly",
                message: "&#128077; Displaying Monthly Checklist",
              },
            },
          },
          apps: {
            label: "Apps",
            path: "/menu/apps",
            command: "apps",
            submenu: {
              record: {
                label: "Record",
                command: "record",
                path: "/menu/apps/record",
                message: "&#128077; Opening Recording App",
              },
              breakout: {
                label: "Breakout",
                command: "breakout",
                path: "/menu/apps/breakout",
                message: "&#128077; Opening Breakout App",
              },
              statistics: {
                label: "Statistics",
                command: "statistics",
                path: "/menu/apps/statistics",
                message: "&#128077; Opening Statistics App",
              },
            },
          },
        },
      },
    },
  },
};

let logId = 0;

// Create WebSocket connection.
const socket = new WebSocket(`wss://192.168.151.52:443`);

// init application
function init() {
  // build navigation
  const appEl = document.getElementById("app");
  const navigationEl = document.createElement("nav");
  const listEl = document.createElement("ul");
  navigationEl.id = "navigation";
  navigationEl.appendChild(listEl);
  appEl.appendChild(navigationEl);

  // navigate to current path or root
  const pathname = window.location.pathname;
  if (pathname) navigateToPath(pathname);
  else navigateToPath("/");

  // Add a popstate event listener to the window object
  window.addEventListener("popstate", function (event) {
    var state = event.state;

    // Load the appropriate page based on the state object
    // without pushing it to the history
    if (state) navigateToPath(state.pageUrl, false);
    else navigateToPath("/", false);
  });

  // adding controls
  if (config.controls.mouse) addMouseControls();
  if (config.controls.button) addButtonControls();
  if (config.controls.voice) addVoiceControls();
  if (config.controls.motion && "RelativeOrientationSensor" in window)
    addMotionControls();

  // listen for WebSocket messages
  socket.addEventListener("message", (event) => {
    let message = JSON.parse(event.data);

    // perform action without broadcasting message again
    if (message.type === "select") navigateToPath(message.value, true, false);
    else if (message.type === "focus")
      focusNavigationItem(message.value, false);
  });
}

// update navigation
function buildPageContents(object) {
  // handle message content
  if (object.hasOwnProperty("message")) {
    // create content element
    const appEl = document.getElementById("app");
    const contentEl = document.createElement("div");
    contentEl.id = "content";
    contentEl.innerHTML = object.message;
    appEl.prepend(contentEl);
  } else {
    // remove content element
    const contentEl = document.getElementById("content");
    if (contentEl) contentEl.remove();
  }

  updateNavigation();

  // update navigation
  function updateNavigation() {
    const listEl = document.createElement("ul");
    const listItems = []; // for setting animation

    // add navigation items
    for (let key in object.submenu) {
      let itemEl = buildNavigationItemEl(object.submenu[key]);
      itemEl.classList.add("forward-btn");
      listEl.appendChild(itemEl);
      listItems.push(itemEl);
    }

    // add "special back" button
    if (object.path !== "/") {
      // determine depth of object
      const segments = object.path.split("/");
      const depth = segments.length - 1;
      let backItemEl;
      if (depth === 1)
        backItemEl = buildNavigationItemEl({
          label: "Close",
          path: "/",
          command: "close",
        });
      else if (depth === 2)
        backItemEl = buildNavigationItemEl({
          label: "Back",
          path: "/menu",
          command: "back",
        });
      else if (depth === 3) {
        backItemEl = buildNavigationItemEl({
          label: "Exit",
          path: "/",
          command: "exit",
        });
        backItemEl.classList.add("exit-btn");
      }
      backItemEl.classList.add("back-btn");
      listEl.appendChild(backItemEl);
      listItems.push(backItemEl);
    }

    // add animation delay
    let index = 0;
    listItems.forEach((item) => {
      item.style.animationDelay = index * 0.07 + "s";
      index++;
    });

    const navigationEl = document.getElementById("navigation");
    navigationEl.replaceChildren(listEl);

    // dispatch custom event to listen for menu updates
    const updated = new CustomEvent("updated");
    navigationEl.dispatchEvent(updated);

    // auto select first item
    // focusNavigationItem(1);

    // build navigation item
    function buildNavigationItemEl(item) {
      const itemEl = document.createElement("li");
      itemEl.className = "link";
      const linkEl = document.createElement("a");
      linkEl.href = item.path;
      linkEl.innerHTML = item.label;
      linkEl.dataset.command = item.command;
      itemEl.appendChild(linkEl);

      const progressEl = document.createElement("div");
      progressEl.className = "progress";
      linkEl.appendChild(progressEl);
      progressEl.addEventListener("animationend", (event) => {
        navigateToSelected();
      });

      if (item.label.toLowerCase() !== item.command.toLowerCase()) {
        const commandEl = document.createElement("div");
        commandEl.className = "command";
        commandEl.innerHTML = `<span><img src="/src/media/mic.svg" alt="mic"/><span>${item.command}</span></span>`;
        itemEl.appendChild(commandEl);
      }

      return itemEl;
    }
  }
}

// general navigation
// ===================

// navigate to path
function navigateToPath(path, pushState = true, broadcast = true) {
  // find object with path
  let navObj = helpers.findNestedObject(navigation, "path", path);
  if (!navObj) navObj = navigation.home; // fallback to home
  buildPageContents(navObj);

  // State Object
  var stateObj = {
    pageTitle: navObj.label,
    pageUrl: navObj.path,
  };

  // Push state to browser history
  if (pushState)
    history.pushState(stateObj, stateObj.pageTitle, stateObj.pageUrl);

  // broadcast selection event to other clients
  if (broadcast && socket.readyState === WebSocket.OPEN)
    socket.send(JSON.stringify({ type: "select", value: path }));
}

// navigate to currently selected item
function navigateToSelected() {
  const currentItem = document.querySelector(
    "#navigation > ul > li[data-selected] a"
  );
  if (currentItem) {
    const path = currentItem.getAttribute("href");
    navigateToPath(path);
  } else console.log("no item selected to open");
}

// focus specific navigation item
function focusNavigationItem(index, broadcast = true) {
  // unselect all items
  const navigationList = document.querySelectorAll("#navigation li");
  for (let item of navigationList) {
    delete item.dataset.selected;
    item.querySelector(".progress").style.animation = "";
  }

  // select requested element
  const navigationItem = document.querySelector(
    `#navigation > ul > li:nth-child(${index})`
  );
  if (navigationItem) navigationItem.dataset.selected = "";

  // broadcast focus event to other clients
  if (broadcast && socket.readyState === WebSocket.OPEN)
    socket.send(JSON.stringify({ type: "focus", value: index }));

  return navigationItem;
}

// focus next navigation item
function focusNextNavigationItem() {
  // get currently selected element
  let currentItem = document.querySelector(
    "#navigation > ul > li[data-selected]"
  );

  if (!currentItem) focusNavigationItem(1);
  else if (currentItem.nextSibling) {
    let nextItem = currentItem.nextSibling;
    delete currentItem.dataset.selected;
    nextItem.dataset.selected = "";
    let index = Array.from(currentItem.parentNode.children).indexOf(nextItem);
    focusNavigationItem(index + 1);
  } else focusNavigationItem(1);
}

// focus previous navigation item
function focusPreviousNavigationItem() {
  // get currently selected element
  let currentItem = document.querySelector(
    "#navigation > ul > li[data-selected]"
  );

  if (!currentItem) focusNavigationItem(1);
  else if (currentItem.previousSibling) {
    let prevItem = currentItem.previousSibling;
    delete currentItem.dataset.selected;
    prevItem.dataset.selected = "";
    let index = Array.from(currentItem.parentNode.children).indexOf(prevItem);
    focusNavigationItem(index + 1);
  } else {
    let childCount = document.querySelector("#navigation > ul").children.length;
    focusNavigationItem(childCount);
  }
}

// focus closest item to point
function focusClosestItem(targetY = window.innerHeight / 2) {
  targetY = targetY + window.innerHeight / 2;
  let lastDistance = 10000;
  let index = 1;
  let closestIndex = 0;
  const itemList = document.querySelectorAll("#navigation ul li");
  for (let item of itemList) {
    let rect = item.getBoundingClientRect();
    let itemCenterY = rect.top + rect.height / 2;
    let distance = Math.abs(targetY - itemCenterY);
    if (distance <= lastDistance) {
      closestIndex = index;
      lastDistance = distance;
    }
    index++;
  }
  return focusNavigationItem(closestIndex);
}

// deselect all items
function deselectAllItems() {
  const navigationList = document.querySelectorAll("#navigation li");
  for (let item of navigationList) {
    item.querySelector(".progress").style.animation = "";
    delete item.dataset.selected;
  }
}

// moving up navigation tree
function moveUpNavigationLevel() {
  const path = window.location.pathname;
  let parentPath = path.slice(0, path.lastIndexOf("/"));
  navigateToPath(parentPath);
}

// moving down navigation tree
function moveDownNavigationLevel() {
  const currentItem = document.querySelector(
    "#navigation > ul > li[data-selected]"
  );
  if (currentItem) {
    const isBackButton = currentItem.classList.contains("back-btn");
    if (!isBackButton) navigateToSelected();
  }
}

// add mouse controls
// ==================
function addMouseControls() {
  const appEl = document.getElementById("app");

  // focus on mouseover
  appEl.addEventListener("mouseover", (event) => {
    const listItem = event.target.closest("li.link");
    if (listItem) {
      // get childnode index
      let index = Array.from(listItem.parentNode.children).indexOf(listItem);
      let itemEl = focusNavigationItem(index + 1);
    }
  });

  // unfocus all items
  appEl.addEventListener("mouseout", (event) => {
    const listItem = event.target.closest("li.link");
    if (listItem) deselectAllItems();
  });

  // select on mouseclick
  appEl.addEventListener("click", (event) => {
    event.preventDefault();
    const listItem = event.target.closest("li.link");
    if (listItem) {
      const link = listItem.querySelector("a");
      const path = link.getAttribute("href");
      navigateToPath(path);
    }
  });
}

// add button controls
// ===================
function addButtonControls() {
  // trying to access the audio buttons of android device
  // let audioButton = document.getElementById("load-audio-btn");
  // console.log(audioButton);
  // audioButton.addEventListener("click", (event) => {

  //   console.log("load audio");
  //   // create an AudioContext object
  //   var audioCtx = new AudioContext();

  //   // create an HTMLAudioElement
  //   var audio = new Audio("/src/media/White-Noise.mp3");

  //   // create an AudioBufferSourceNode
  //   var source = audioCtx.createMediaElementSource(audio);

  //   //audio.play();

  //   // connect the AudioBufferSourceNode to the AudioContext's destination
  //   source.connect(audioCtx.destination);

  //   // add an event listener to the 'volumechange' event
  //   audio.addEventListener("volumechange", function (event) {
  //     console.log("volume change event");
  //     // get the new volume value
  //     var volume = audio.volume;

  //     // set the gain of the AudioBufferSourceNode to the new volume value
  //     var gainNode = audioCtx.createGain();
  //     gainNode.gain.value = volume;
  //     source.connect(gainNode);
  //     gainNode.connect(audioCtx.destination);
  //   });
  // });

  // add two button controls for menu
  document.addEventListener("keydown", onKeyDown);

  // on keydown event
  function onKeyDown(event) {
    // prevent defaults
    if (
      event.code === "Tab" ||
      event.code === "Enter" ||
      event.code === "Escape" ||
      event.code === "ArrowUp" ||
      event.code === "ArrowRight" ||
      event.code === "ArrowDown" ||
      event.code === "ArrowLeft" ||
      event.code === "ShiftRight"
    )
      event.preventDefault();

    // browsing navigation items
    if (event.code === "Tab" || event.code === "ArrowDown")
      focusNextNavigationItem();
    else if (event.code === "ArrowUp") focusPreviousNavigationItem();
    else if (event.code === "Enter" || event.code === "ShiftRight")
      navigateToSelected();
    else if (event.code === "ArrowRight") moveDownNavigationLevel();
    else if (event.code === "ArrowLeft" || event.code === "Escape")
      moveUpNavigationLevel();
  }

  // const toggleButtonControlsEl = document.getElementById(
  //   "toggle-button-controls"
  // );

  // toggleButtonControlsEl.addEventListener("click", (event) => {
  //   const audioElement = document.createElement("audio");
  //   const audioCtx = new AudioContext();
  //   const source = audioCtx.createMediaElementSource(audioElement);
  //   const gainNode = audioCtx.createGain();
  //   source.connect(gainNode).connect(audioCtx.destination);

  //   // Set the initial volume level
  //   gainNode.gain.value = 0.2;
  //   console.log(gainNode.gain.value);

  // Adjust the volume when the device volume buttons are pressed
  // document.addEventListener("keydown", (event) => {
  //   if (event.key === "ArrowUp") {
  //     printLogMsg("volume up");
  //     gainNode.gain.value += 0.1;
  //   } else if (event.key === "ArrowDown") {
  //     printLogMsg("volume down");
  //     gainNode.gain.value -= 0.1;
  //   }
  // });
  // });
}

// add voice controls
// ===================
function addVoiceControls() {
  // voice controls only work on google chrome!
  var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
  var SpeechGrammarList = SpeechGrammarList || window.webkitSpeechGrammarList;
  var SpeechRecognitionEvent =
    SpeechRecognitionEvent || webkitSpeechRecognitionEvent;

  let availableCommands = [];
  let commands = getVoiceCommandList();

  // listen for menu updates and refresh commands list
  const navigationEl = document.getElementById("navigation");
  navigationEl.addEventListener("updated", (event) => {
    commands = getVoiceCommandList();
  });

  let recognition = new SpeechRecognition();
  if (SpeechGrammarList) {
    var speechRecognitionList = new SpeechGrammarList();
    var grammar =
      "#JSGF V1.0; grammar commands; public <command> = " +
      commands.join(" | ") +
      " ;";
    speechRecognitionList.addFromString(grammar, 1);
    // recognition.grammars = speechRecognitionList;
  }
  recognition.continuous = false;
  recognition.lang = "en-US";
  recognition.interimResults = false;
  recognition.maxAlternatives = 1;

  recognition.start();
  console.log("Voice > listening");

  // restart recognition after ended automatically
  recognition.addEventListener("end", () => {
    recognition.start();
  });

  recognition.onresult = function (event) {
    let command = event.results[0][0].transcript.toLowerCase();
    let confidence = event.results[0][0].confidence;

    console.log(`Voice > Received "${command}" (${confidence.toFixed(2)})`);

    const matchingItem = commands.find((item) => command.includes(item));
    if (matchingItem) {
      let index = commands.indexOf(matchingItem); // get index of command in array
      let path = availableCommands[index].path; // get corresponding path in availableCommands array
      navigateToPath(path);
    } else if (command.includes("select") || command.includes("okay"))
      navigateToSelected();
    else if (command.includes("open")) moveDownNavigationLevel();
    else if (command.includes("emergency"))
      navigateToPath("/menu/call/emergency");
    else if (command.includes("exit") || command.includes("home"))
      navigateToPath("/");
    else if (command.includes("back")) moveUpNavigationLevel();
    else if (command.includes("next") || command.includes("down"))
      focusNextNavigationItem();
    else if (command.includes("previous") || command.includes("up"))
      focusPreviousNavigationItem();
  };

  // updating voice command list
  function getVoiceCommandList() {
    // search and save currently available voice commands
    availableCommands = [];
    const listItems = document.querySelectorAll("a[data-command]");
    for (let item of listItems) {
      let command = {
        command: item.dataset.command,
        path: item.getAttribute("href"),
      };
      availableCommands.push(command);
    }

    // form commands array for grammarlist
    return availableCommands.map((x) => x.command);
  }
}

// add motion controls
// ===================
function addMotionControls() {
  let referenceY; // current zero reference point
  let factor = -5000; // factor to map sensor data to pixels (sensitivity)
  let screenCenter = new Point(window.innerWidth / 2, window.innerHeight / 2);
  let idleTimeout;
  let idleDelay = 4000; //idle time before reseting reference point
  let autoSelect = false; // selecting items with time delay
  let drawMotionFlag = true; // draw motion values for debugging

  // motion buffer stuff
  let motionBuffer = []; // motion value buffer
  let averageY = 0;
  let lastAverageY = 0; // last average of buffer values
  let bufferIterations = 0; // how many times buffer has been iterated
  let targetPositionInPx = 0;
  let currentPositionInPx = 0;
  let lastPositionInPx = 0;
  const bufferIterationLimit = 5; // iterations before new average calculation is triggered
  const bufferLength = 10; // length of value buffer
  const noiseThreshold = 30; // threshold in pixel to avoid sensor noise
  const innerLimit = 50; // threshold in pixel to define inactive area around the reference
  const outerLimit = 400;

  // create canvas element for motion visualization
  const canvasEl = document.createElement("canvas");
  canvasEl.id = "canvas";
  canvasEl.width = window.innerWidth;
  canvasEl.height = window.innerHeight;
  document.getElementById("app").appendChild(canvasEl);

  //available headset sensors: accelerometer, gyroscope, relativeOrientation, linearAcceleration, GravitySensor
  //unavailable headset sensors: magnetometer (this results in horizontal drifting)
  const options = { frequency: 30, referenceFrame: "device" };
  const sensor = new RelativeOrientationSensor(options);
  sensor.addEventListener("reading", onChangeOrientation);

  // get sensor permission via Permission API
  Promise.all([
    navigator.permissions.query({ name: "accelerometer" }),
    navigator.permissions.query({ name: "gyroscope" }),
  ]).then((results) => {
    if (results.every((result) => result.state === "granted")) sensor.start();
    else printLogMsg("No permissions to use RelativeOrientationSensor.");
  });

  // handle motion sensor event
  function onChangeOrientation(event) {
    let quaternion = event.target.quaternion;
    let y = quaternion[1];

    // write quaternion y value into motion buffer
    motionBuffer.push(y);

    // on first iteration
    if (!referenceY) {
      referenceY = y; // set initial reference point
      averageY = y; // set initial last average point
      lastAverageY = y; // set initial last average point
      lastPositionInPx = (averageY - referenceY) * factor;
    }

    // calculate positions in pixels
    targetPositionInPx = (averageY - referenceY) * factor; // target position in pixels
    currentPositionInPx =
      lastPositionInPx + (targetPositionInPx - lastPositionInPx) * 0.1; // move towards target
    currentPositionInPx = helpers.clamp(
      currentPositionInPx,
      -outerLimit,
      outerLimit
    ); // clamp to reasonable range
    lastPositionInPx = currentPositionInPx; // save current position for later

    // draw sensor motion for debugging
    if (drawMotionFlag) drawMotion(currentPositionInPx);

    // select item at target position if conditions are met
    let averageDiffInPx = Math.abs((lastAverageY - averageY) * factor);
    let distFromCenter = Math.abs(currentPositionInPx);
    // if position is withing limits
    if (distFromCenter < outerLimit && distFromCenter > innerLimit) {
      // movement is not noise
      if (averageDiffInPx > noiseThreshold) {
        let itemEl = focusClosestItem(currentPositionInPx);
        lastAverageY = averageY; // save averageY for later comparison

        // trigger autoselection via motion
        if (autoSelect) {
          let progressEl = itemEl.querySelector(".progress");
          progressEl.style.animation = "progress 2s ease-out forwards";
        }

        // handle auto reseting of reference point
        clearTimeout(idleTimeout);
        idleTimeout = setTimeout(() => {
          referenceY = averageY;
        }, idleDelay);
      }
    } else deselectAllItems();

    // fill buffer with datapoints
    if (motionBuffer.length > bufferLength) {
      motionBuffer.shift(); // limit buffer length

      // calculate the new average value as soon as bufferiterationlimit reached
      if (bufferIterations > bufferIterationLimit) {
        averageY = motionBuffer.reduce((a, b) => a + b) / motionBuffer.length;
        bufferIterations = 0;
      }
      bufferIterations++;
    }
  }

  function drawMotion(y) {
    if (canvasEl.getContext) {
      const ctx = canvasEl.getContext("2d");
      const pointSize = 10;

      ctx.clearRect(0, 0, canvasEl.width, canvasEl.height);

      // save zone
      ctx.beginPath();
      ctx.arc(screenCenter.x, screenCenter.y, innerLimit, 0, 2 * Math.PI);
      ctx.strokeStyle = "rgba(255, 0, 255, .6)";
      ctx.lineWidth = 3;
      ctx.stroke();
      ctx.closePath();

      // outer limit
      ctx.beginPath();
      ctx.arc(screenCenter.x, screenCenter.y, outerLimit, 0, 2 * Math.PI);
      ctx.strokeStyle = "rgba(255, 0, 255, .6)";
      ctx.lineWidth = 3;
      ctx.stroke();
      ctx.closePath();

      // distance
      ctx.beginPath();
      ctx.moveTo(screenCenter.x, screenCenter.y);
      ctx.lineTo(screenCenter.x, screenCenter.y + y);
      ctx.strokeStyle = "rgba(255, 0, 255, .6)";
      ctx.lineWidth = 3;
      ctx.stroke();
      ctx.closePath();

      // target
      ctx.beginPath();
      ctx.arc(screenCenter.x, screenCenter.y + y, pointSize, 0, 2 * Math.PI);
      ctx.fillStyle = "rgba(255, 0, 255, .6)";
      ctx.fill();
      ctx.closePath();
    }
  }
}

// printing messages on screen for debugging
function printLogMsg(message) {
  const logEl = document.getElementById("log");
  if (logEl.scrollHeight >= 10000) logEl.innerHTML = "";
  logEl.innerHTML += `${logId}: ${message}<br>`;
  logEl.scrollTop = logEl.scrollHeight;
  logId++;
  console.log(message);
}

init();
